﻿namespace FizzBuzz.Printers
{
    public interface IPrinter<out T>
    {
        T Context { get; }
        
        public void Print();
    }
}