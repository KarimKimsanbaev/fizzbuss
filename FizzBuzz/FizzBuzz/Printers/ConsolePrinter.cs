﻿using System;
using FizzBuzz.Tags;

namespace FizzBuzz.Printers
{
    public class ConsolePrinter : IPrinter<ITag>
    {
        private readonly ITag _context;
        public ITag Context => _context;
        
        public ConsolePrinter(ITag context)
        {
            _context = context;
        }

        public void Print()
        {
            Console.WriteLine(_context.Value);
        }
    }
}