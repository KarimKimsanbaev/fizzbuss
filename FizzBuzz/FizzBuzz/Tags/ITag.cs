﻿using FizzBuzz.Shared;

namespace FizzBuzz.Tags
{
    public interface ITag : IValue<string>
    {
    }
}