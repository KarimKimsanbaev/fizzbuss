﻿using FizzBuzz.Strategy;
using FizzBuzz.Tags;

namespace FizzBuzz.Rules
{
    public interface IRule
    {
        ITag Tag { get; }
        IStrategy Strategy { get; }

        bool IsSuccess(int num);
    }
}