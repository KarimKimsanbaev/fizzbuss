﻿using System.Collections.Generic;
using FizzBuzz.Rules;
using FizzBuzz.Tags;

namespace FizzBuzz.RuleCollections
{
    public class TagNameRuleCollection : IRuleCollection
    {
        private readonly List<IRule> _tags;
        public List<IRule> Tags => _tags;

        public TagNameRuleCollection(List<IRule> tags)
        {
            _tags = tags;
        }

        public ITag Find(int num, ITag defaultValue)
        {
            foreach (var tag in _tags)
            {
                if (tag.IsSuccess(num))
                {
                    return tag.Tag;
                }
            }

            return defaultValue;
        }
    }
}