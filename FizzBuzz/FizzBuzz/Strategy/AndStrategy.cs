﻿using System.Collections.Generic;
using FizzBuzz.Shared;

namespace FizzBuzz.Strategy
{
    public class AndStrategy : IStrategy
    {
        private readonly List<ITruthy> _conditions;
        public List<ITruthy> Conditions => _conditions;
        
        public AndStrategy(List<ITruthy> conditions)
        {
            _conditions = conditions;
        }
        
        public bool IsTruthy(int num)
        {
            foreach (var condition in _conditions)
            {
                if (!condition.IsTruthy(num))
                {
                    return false;
                }
            }

            return true;
        }

        
    }
}