﻿using System.Collections.Generic;
using FizzBuzz.Shared;

namespace FizzBuzz.Strategy
{
    public interface IStrategy : ITruthy
    {
        List<ITruthy> Conditions { get; }
    }
}